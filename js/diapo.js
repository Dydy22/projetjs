let prev = document.getElementById('prev');
let next = document.getElementById('next');
let item = document.getElementById('item');
let validate = document.getElementById('validate');
let answerInput = document.getElementById('answer');
let scoreDisplay = document.getElementById('score');
let counter = 0;
let score = 0;

let countries = ['Canada', 'Niger', 'Mexique', 'Suisse', 'Inde', 'Japon', 'Côte d\'Ivoire', 'Bolivie', 'Tunisie', 'Italie'];
let capitals = ['Ottawa', 'Niamey Côte', 'Mexico', 'Bern', 'NewDelhi', 'Tokyo', 'Yamoussoukro', 'Sucre', 'Tunis', 'Rome'];
let answers = [];

window.addEventListener('load', init);

function init() {
    item.children[0].src = 'img/' + counter + '.jpg';
    displayQuestion(counter);
    prev.addEventListener('click', prevItem);
    next.addEventListener('click', nextItem);
    validate.addEventListener('click', validateCurrentQuestion);
    answerInput.addEventListener('keyup', function (event) {
        if (event.key === 'Enter') {
            validateCurrentQuestion();
        }
    });
}

function prevItem() {
    counter--;
    counter = (counter == -1) ? 9 : counter;
    displayImage();
    displayQuestion(counter);
}

function nextItem() {
    counter++;
    counter = (counter == 10) ? 0 : counter;
    displayImage();
    displayQuestion(counter);
}

function displayImage() {
    item.children[0].style.opacity = 0;
    item.addEventListener('transitionend', function () {
        console.log('transition terminée');
        item.children[0].src = 'img/' + counter + '.jpg';
        item.children[0].style.opacity = 1;
    })

    answerInput.focus();
    answerInput.value = "";

    // Si la réponse existe c'est qu'elle est valide, on met la réponse dans l'input
    if (answers[counter]) {
        answerInput.value = answers[counter];
    }
}

function displayQuestion(counter) {
    document.getElementById('country-name').innerHTML = countries[counter];
}

function validateCurrentQuestion() {
    let questionIndex = counter;
    let answer = document.getElementById('answer').value;

    if (answer == '') {
        alert("Veuillez écrire une réponse!");

        return;
    }

    nextItem();

    if (capitals[questionIndex].toLowerCase() == answer.toLowerCase()) {
        answers[questionIndex] = answer;
        updateScore();
    }
}

function updateScore() {
    score++;
    scoreDisplay.innerHTML = score;

    // Si toutes les réponses sont trouvés
    if (score == countries.length) {
        scoreDisplay.innerHTML = "Bravo!";
        document.getElementById('score-wrapper').classList.add('success');
    }
}


