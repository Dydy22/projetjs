function updateInput() {
    var prixProduit = 10;

    let quantite = document.getElementById("quantite").value;
    let totalSansTaxe = quantite * prixProduit;

    let tvq = (totalSansTaxe * 9.975) / 100;
    let tps = (totalSansTaxe * 5) / 100;
    let total = totalSansTaxe + tvq + tps;

    document.getElementById("totalSansTaxe").innerHTML = totalSansTaxe;
    document.getElementById("tps").innerHTML = tps;
    document.getElementById("tvq").innerHTML = tvq;
    document.getElementById("total").innerHTML = total;
}

function envoyerCommande() {

    let listeChamp = ['prenom', 'nom', 'adresse', 'codePostal', 'ville', 'jourLivraison', 'pays', 'quantite', 'cardNumber'];
    let errors = 0;

    for (let i = 0; i < listeChamp.length; i++) {
        let champ = document.getElementById(listeChamp[i]);

        if (champ.value == "" || champ.value == 0) {
            champ.classList.add("error");
            errors++;
        } else {
            champ.classList.remove("error");
        }
    }

    if (errors == 0) {
        alert("Merci pour votre commande!");
    } else {
        alert("Tous les champs sont obligatoires (" + errors + " erreurs)");
    }
}